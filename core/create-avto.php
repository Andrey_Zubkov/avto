<?php 
function avto_init() {
    $labels = array(
        'name'                  => 'Машины',
        'singular_name'         => 'Машина',
        'menu_name'             => 'Машины',
        'name_admin_bar'        => 'Машина',
        'add_new'               => 'Добавить машину',
        'add_new_item'          => 'Добавить новую машину',
        'new_item'              => 'Новая машина',
        'edit_item'             => 'Редактировать машину',
        'all_items'             => 'Все машины',
        'search_items'          => 'Найти товар',
        'not_found'             => 'Машина не найдена',
        'not_found_in_trash'    => 'Машин нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'has_archive'           => false,
        'menu_position'         => 4,
        'menu_icon'             => 'dashicons-admin-multisite',
        'supports'              => array('title', 'editor', 'thumbnail'),
    );

    register_post_type( 'avto' , $args);
}
add_action( 'init', 'avto_init' );

function create_taxonomy(){
    // заголовки
    $labels = array(
        'name'              => 'Категории машин',
        'singular_name'     => 'Категория машины',
        'search_items'      => 'Найти категории',
        'all_items'         => 'Все категории',
        'parent_item'       => 'Категория',
        'parent_item_colon' => 'Категория:',
        'edit_item'         => 'Редактировать данные категории машины',
        'update_item'       => 'Обновить данные категории машины',
        'add_new_item'      => 'Добавить новую категорию машины',
        'new_item_name'     => 'Новая категория машины',
        'menu_name'         => 'Категория',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('avto-cat', array('avto'), $args );
}
add_action('init', 'create_taxonomy');

function create_taxonomy_mark(){
    // заголовки
    $labels = array(
        'name'              => 'Марка машин',
        'singular_name'     => 'Марка машины',
        'search_items'      => 'Найти Марку',
        'all_items'         => 'Все марки',
        'parent_item'       => 'Марка',
        'parent_item_colon' => 'Марка:',
        'edit_item'         => 'Редактировать данные марки машины',
        'update_item'       => 'Обновить данные марки машины',
        'add_new_item'      => 'Добавить новую марку машины',
        'new_item_name'     => 'Новая марка машины',
        'menu_name'         => 'Марка',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('avto-mark', array('avto'), $args );
}
add_action('init', 'create_taxonomy_mark');

add_action('add_meta_boxes', 'my_extra_fields', 1);

//Добавление произвольных полей к товарам и фасадам
function my_extra_fields() {
    add_meta_box( 'extra_fields', 'Информация о машине', 'fields_avto', 'avto', 'normal', 'high'  );
}

function fields_avto($post) {
    $images = json_decode(get_post_meta( $post->ID, 'images', 1 ));

    ?>

    <?php if (isset($images)): ?>
        <?php foreach ($images as $key => $images): ?>
            <img style="height: 100px; width:100px;" src="<?= $images; ?>" alt="" />
        <?php endforeach; ?>
    <?php endif; ?>  
    <p>
        <label>Год</label>
        <input type="text" name="avto[year]" value="<?= get_post_meta($post->ID, 'year', 1); ?>"/>
    </p>
    <p>
        <label>КПП</label>
        <select name="avto[kpp]">
            <option value="механика" <?php selected( get_post_meta($post->ID, 'kpp', 1), 'механика'); ?>>Механика</option>
            <option value="автомат" <?php selected( get_post_meta($post->ID, 'kpp', 1), 'автомат'); ?>>Автомат</option>
        </select>
    </p>
    <p>
        <label>Топливо</label>
        <input type="text" name="avto[toplivo]" size="100" value="<?= get_post_meta($post->ID, 'toplivo', 1); ?>"/>
    </p>
    <p>
        <label>Объем двигателя</label>
        <input type="text" name="avto[engine]" size="100" value="<?= get_post_meta($post->ID, 'engine', 1); ?>"/>
    </p>
    <p>
        <label>Мощьность</label>
        <input type="text" name="avto[powerfull]" size="100" value="<?= get_post_meta($post->ID, 'powerfull', 1); ?>"/>
    </p>
    <p>
        <label>Салон</label>
        <input type="text" name="avto[salon]" size="100" value="<?= get_post_meta($post->ID, 'salon', 1); ?>"/>
    </p>
    <p>
        <label>Цвет</label>
        <input type="text" name="avto[color]" size="100" value="<?= get_post_meta($post->ID, 'color', 1); ?>"/>
    </p>
    <h3>Тарифы</h3>
    <p>
        <label>1 сутки</label>
        <input type="text" name="avto[tarif_1]" size="100" value="<?= get_post_meta($post->ID, 'tarif_1', 1); ?>"/>
    </p>
    <p>
        <label>от 2 до 8 суток</label>
        <input type="text" name="avto[tarif_2]" size="100" value="<?= get_post_meta($post->ID, 'tarif_2', 1); ?>"/>
    </p>
    <p>
        <label>от 9 до 15 суток</label>
        <input type="text" name="avto[tarif_3]" size="100" value="<?= get_post_meta($post->ID, 'tarif_3', 1); ?>"/>
    </p>
    <p>
        <label>от 16 до 30 суток</label>
        <input type="text" name="avto[tarif_4]" size="100" value="<?= get_post_meta($post->ID, 'tarif_4', 1); ?>"/>
    </p>
    <p>
        <label>от 31 дня</label>
        <input type="text" name="avto[tarif_5]" size="100" value="<?= get_post_meta($post->ID, 'tarif_5', 1); ?>"/>
    </p>
    <p>
        <label>безлимитный тариф</label>
        <input type="text" name="avto[tarif_6]" size="100" value="<?= get_post_meta($post->ID, 'tarif_6', 1); ?>"/>
    </p>
    <p>
        <label>цена за 1км перепробега</label>
        <input type="text" name="avto[coast_km]" size="100" value="<?= get_post_meta($post->ID, 'coast_km', 1); ?>"/>
    </p>
    <p>
        <label>залог</label>
        <input type="text" name="avto[zalog]" size="100" value="<?= get_post_meta($post->ID, 'zalog', 1); ?>"/>
    </p>
    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

    <input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

    <?php
}
//Сохранение произвольных полей товаров
add_action('save_post', 'update_fields_avto', 0);
/* Сохраняем данные, при сохранении поста */
function update_fields_avto($post_id ) {

    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
    if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

    if( !isset($_POST['avto']) ) return false;

   
    foreach( $_POST['avto'] as $key=>$value ){
        if( empty($value) && $key != 'images' ){
            delete_post_meta($post_id, $key); // удаляем поле если значение пустое
            continue;
        }
        if($key == 'images' && !empty($value)){
        update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
            
     }
        if($key !='images')
            update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
    }
    return $post_id;
}

// Add gallery for avto
function avto_images_extra_fields() {
    add_meta_box(
        'side_images_product',
        'Изображения машины',
        'avto_side_images_fields',
        'avto',
        'side',
        'low'
    );
}
add_action( 'add_meta_boxes', 'avto_images_extra_fields', 1 );

function avto_side_images_fields($post) {
    wp_enqueue_media(); ?>


    <p>
        <button class="button button-primary add_images">Добавить изображения</button>
        <input class="input_images" type="hidden" name="avto[images]" value="">
        </p>
    <script>
        jQuery(document).ready(function(){

            jQuery('.add_images').click(function(e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: true
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first();
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Output to the console uploaded_image
                        var array_object = image.state().get('selection').toJSON();

                        var image_url_array = [];
                        if(array_object.length > 7) {

                            var image_urls = '';
                        }
                        else {
                            for(var i = 0; i < array_object.length; i++) {
                                image_url_array.push(array_object[i].url);
                            }

                            var image_urls = JSON.stringify(image_url_array);
                        }
                        // Let's assign the url value to the input field
                        jQuery('.input_images').val(image_urls);
                    });
            });
        });
    </script>

    <?php
}

function avto_custom_column($defaults) {

    $columns = array(
        'img'           => '<span class="dashicons dashicons-format-image"></span>',        
        'title'         => 'Название',
        'category'      => 'Категория',
        'mark'          => 'Марка',
        'date'          => 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_avto_posts_columns', 'avto_custom_column' );

/*
 * Get value to column fields
 */
//Получение произвольных колонок к товарам
function avto_get_column_value($column, $post_id) {
    if( $column == 'img' )
        if (get_the_post_thumbnail( $post_id, array( 60, 60 ) ))
            echo get_the_post_thumbnail( $post_id, array( 60, 60 ) );
        else
            echo 'Изображение не задано.';    
    if( $column == 'category') {
       $category = get_the_terms( $post_id, 'avto-cat');
       if($category) {
            foreach ($category as $value) {
                echo $value->description.' '.$value->name.',';
            }
        } else {
            echo 'Категория машины не указана';
        }
    }
    if( $column == 'mark') {
       $mark = get_the_terms( $post_id, 'avto-mark');
        if(isset($mark[0]->name) && !empty($mark[0]->name)) {
            echo $mark[0]->name;
        } else {
            echo 'Марка машины не указана';
        }
    }
 
    
}
add_action( 'manage_avto_posts_custom_column', 'avto_get_column_value', 10, 2 );

add_action("avto-cat_edit_form_fields", 'add_new_custom_fields_avto_cat');
function add_new_custom_fields_avto_cat($term){
    wp_enqueue_media();
    $photo_cat = get_term_meta( $term->term_id, 'photo_cat_avto', 1 );
    if(isset($photo_cat) && !empty($photo_cat)) {
        echo '<img src="'.$photo_cat.'" style="width:150px; height:100px;"> ';
        echo '<input type="submit" name="delete_photo" value="Удалить">';
    } else {
    ?>

    <div>
        <h2>Изображение</h2>
        <button id="add_img" class="add_img">Добавить</button>
        <input  id="uploadImage" type="hidden" name="photo_cat_avto" value="">
    </div> 
    <?php } ?>       

    <script>
        jQuery(document).ready(function($){

            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        jQuery('#uploadImage').val(uploaded_image);
                    });
            });

        });
    </script>


<?php 
}

//Обновление произвольных полей категории фасадов
function save_custom_taxonomy_meta($term_id){
    $extra = array_map('trim', $_POST['avto-cat']);
    if(isset($_POST['photo_cat_avto']) && !empty($_POST['photo_cat_avto'])) {
        update_term_meta($term_id, 'photo_cat_avto', $_POST['photo_cat_avto']);
    }
    if(isset($_POST['delete_photo']) && !empty($_POST['delete_photo'])) {
        update_term_meta($term_id, 'photo_cat_avto', '');
    }
    foreach( $extra as $key => $value ){
        if( empty($value) ){
            delete_term_meta( $term_id, $key ); // удаляем поле если значение пустое
            continue;
        }

        update_term_meta( $term_id, $key, $value ); // add_term_meta() работает автоматически
    }   

    return $term_id;
}
add_action("edited_avto-cat", 'save_custom_taxonomy_meta');

add_action("avto-mark_edit_form_fields", 'add_new_custom_fields_avto_mark');
function add_new_custom_fields_avto_mark($term){
    wp_enqueue_media();
    $photo_cat = get_term_meta( $term->term_id, 'photo_mark_avto', 1 );
    if(isset($photo_cat) && !empty($photo_cat)) {
        echo '<img src="'.$photo_cat.'" style="width:150px; height:100px;">';
        echo '<input type="submit" name="delete_photo" value="Удалить">';
    } else {
    ?>

    <div>
        <h2>Изображение</h2>
        <button id="add_img" class="add_img">Добавить</button>
        <input  id="uploadImage" type="hidden" name="photo_mark_avto" value="">
    </div> 
    <?php } ?>       

    <script>
        jQuery(document).ready(function($){

            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        jQuery('#uploadImage').val(uploaded_image);
                    });
            });

        });
    </script>


<?php 
}

//Обновление произвольных полей категории фасадов
function save_custom_taxonomy_meta_mark($term_id){
    $extra = array_map('trim', $_POST['avto-cat']);
    if(isset($_POST['photo_mark_avto']) && !empty($_POST['photo_mark_avto'])) {
        update_term_meta($term_id, 'photo_mark_avto', $_POST['photo_mark_avto']);
    }
    if(isset($_POST['delete_photo']) && !empty($_POST['delete_photo'])) {
        update_term_meta($term_id, 'photo_mark_avto', '');
    }
    foreach( $extra as $key => $value ){
        if( empty($value) ){
            delete_term_meta( $term_id, $key ); // удаляем поле если значение пустое
            continue;
        }

        update_term_meta( $term_id, $key, $value ); // add_term_meta() работает автоматически
    }   

    return $term_id;
}
add_action("edited_avto-mark", 'save_custom_taxonomy_meta_mark');
