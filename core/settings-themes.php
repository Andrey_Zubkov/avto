<?php
add_action('admin_menu', 'register_my_custom_submenu_page');
function register_my_custom_submenu_page() {
	add_menu_page( 'Настройки темы', 'Настройки темы', 'manage_options', 'my_theme_options', 'options_print' );
	add_submenu_page( 'my_theme_options', 'Настройки слайдера', 'Настройки слайдера', 'manage_options', 'setting_slider', 'setting_slider_print' );

}

function options_print() {
	?>
	<div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>
		<form action="options.php" method="POST">
			<?php
				settings_fields( 'main-setting' );     // скрытые защитные поля
				do_settings_sections( 'Настройки темы' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
	</div>
	<?php
}

add_action('admin_init', 'main_setting');
/** Add section and field to main setting theme **/
function main_setting() {
	register_setting('main-setting', 'main_option'); //регистрация главных настроек
	add_settings_section('section_info', 'Главные настройки', '', 'Настройки темы');//добавление секции для главных настроек
	add_settings_field('email','Адрес эл. почты', 'fill_input_text','Настройки темы', 'section_info',array('name' 	=> 'email', 'desc' 	=> 'Введите имейл.', 'option' => 'main_option'));
	add_settings_field('phone_1','Телефон', 'fill_input_text','Настройки темы', 'section_info',array('name' 		=> 'phone_1', 'desc' => 'Введите номер телефона 1. В формате +38ХХХХХХХХХХ', 'option' => 'main_option'));
	add_settings_field('address','Адрес', 'fill_input_text','Настройки темы', 'section_info',array('name' 		=> 'address', 'desc' => 'Введите адрес', 'option' => 'main_option'));	
	add_settings_field('work','Режим работы', 'fill_input_text','Настройки темы', 'section_info',array('name' 		=> 'work', 'desc' => 'Введите режим работы', 'option' => 'main_option'));
}
/** field filling the main settings  **/
function fill_input_text($arg) {
	$option = get_option($arg['option']);	
	$name = $arg['name'];
	$desc = $arg['desc'];
	$val = $option[$name];
	?>

	<label>
		<input type="text" name="<?=$arg['option'];?>[<?=$name?>]" size="100px" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description"><?= $desc; ?></p>
	</label>

	<?php
}

function setting_slider_print() {
	?>
	<div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>
		<form action="options.php" method="POST">
			<?php
				settings_fields( 'slider-setting' );     // скрытые защитные поля
				do_settings_sections( 'Настройки слайдера' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
	</div>
	<?php
}

add_action('admin_init', 'slider_setting');
/** Add section and field to main setting theme **/
function slider_setting() {
	register_setting('slider-setting', 'slider_option'); //регистрация главных настроек
	add_settings_section('section_info_slide', 'Настройки слайдера', '', 'Настройки слайдера');//добавление секции для главных настроек
	add_settings_field('photo_slide_1','Фото слайдера 1', 'fill_input_img','Настройки слайдера', 'section_info_slide',array('name' 	=> 'photo_slide_1', 'desc' => 'Добавте фото.', 'option' => 'slider_option', 'number' => 1));
	add_settings_field('photo_slide_2','Фото слайдера 2', 'fill_input_img_2','Настройки слайдера', 'section_info_slide',array('name' 	=> 'photo_slide_2', 'desc' => 'Добавте фото.', 'option' => 'slider_option', 'number' => 2));
	add_settings_field('photo_slide_3','Фото слайдера 3', 'fill_input_img_3','Настройки слайдера', 'section_info_slide',array('name' 	=> 'photo_slide_3', 'desc' => 'Добавте фото.', 'option' => 'slider_option', 'number' => 3));
}
function fill_input_img($arg) {
	$option = get_option($arg['option']);	
	$name = $arg['name'];
	$desc = $arg['desc'];
	$number = $arg['number'];
	$val = $option[$name];
	wp_enqueue_media(); ?>
	<?php if(isset($val) && !empty($val)): ?>
		<img src="<?php echo $val; ?>" style="width:150px; height:100px;">
	<?php endif; ?>
	<p>
    <button class="button button-primary add_img">Добавить изображения</button>
    <input id="<?php echo $name;?>" type="hidden" name="<?php echo $arg['option'];?>[<?php echo $name; ?>]" value="<?php echo $val;?>">
    </p>
	<script>
        jQuery(document).ready(function($){
            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        var id_input = <?php echo $name; ?>;
                        jQuery('#' + $(id_input).attr('id')).val(uploaded_image);
                    });
            });

        });
    </script>
    <?php
}

function fill_input_img_2($arg) {
	$option = get_option($arg['option']);	
	$name = $arg['name'];
	$desc = $arg['desc'];
	$number = $arg['number'];
	$val = $option[$name];
	wp_enqueue_media(); ?>
	<?php if(isset($val) && !empty($val)): ?>
		<img src="<?php echo $val; ?>" style="width:150px; height:100px;">
	<?php endif; ?>
	<p>
    <button class="button button-primary add_img_2">Добавить изображения</button>
    <input id="<?php echo $name;?>" type="hidden" name="<?php echo $arg['option'];?>[<?php echo $name; ?>]" value="<?php echo $val;?>">
    </p>
	<script>
        jQuery(document).ready(function($){
            $('.add_img_2').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        var id_input = <?php echo $name; ?>;
                        jQuery('#' + $(id_input).attr('id')).val(uploaded_image);
                    });
            });

        });
    </script>
    <?php
}

function fill_input_img_3($arg) {
	$option = get_option($arg['option']);	
	$name = $arg['name'];
	$desc = $arg['desc'];
	$number = $arg['number'];
	$val = $option[$name];
	wp_enqueue_media(); ?>
	<?php if(isset($val) && !empty($val)): ?>
		<img src="<?php echo $val; ?>" style="width:150px; height:100px;">
	<?php endif; ?>
	<p>
    <button class="button button-primary add_img_3">Добавить изображения</button>
    <input id="<?php echo $name;?>" type="hidden" name="<?php echo $arg['option'];?>[<?php echo $name; ?>]" value="<?php echo $val;?>">
    </p>
	<script>
        jQuery(document).ready(function($){
            $('.add_img_3').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        var id_input = <?php echo $name; ?>;
                        jQuery('#' + $(id_input).attr('id')).val(uploaded_image);
                    });
            });

        });
    </script>
    <?php
}