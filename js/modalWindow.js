jQuery(document).ready(function ($) {

    var modal = $("#modal");
    var bgModal = $("#bgModal");

    $("#click").on("click", function () {
        $(modal).css({
            display: "block",
        });
        $(bgModal).css({
            display: "block",
        });
        $("#close").on("click", function () {
            $(modal).css({
                display: "none",
            });
            $(bgModal).css({
                display: "none",
            })
        })

    });
});
