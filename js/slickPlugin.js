jQuery(document).ready(function ($) {
    $('.slider-move').slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: '',
        prevArrow: "",
        arrows: true

    });
    $("#chooseCar").slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: "",
        prevArrow: "",
        arrows: true
    });

    $("#up").upScroll();

});