<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body>
<header>
<?php $info_settings = get_option('main_option');?>
    <div class="wrapper holder">
        <div class="flex-container">
            <h1 class="logo" id="top">
                <a href="/">Logo</a>
            </h1>
            <article class="flex-item">
                <span><?php echo __('Тел:','avto_jt');?> <?php echo $info_settings['phone_1']; ?></span><br>
                <span><?php echo __('Email:','avto_jt');?> <?php echo $info_settings['email']; ?></span>
            </article>
            <article class="flex-item">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span><?php echo $info_settings['address']; ?></span><br>
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <span><?php echo $info_settings['work']; ?></span>
            </article>
            <article class="flex-item">
                <select>
                    <option selected>USD</option>
                    <option>RUB</option>
                    <option>EUR</option>
                </select>
                <?php dynamic_sidebar( 'header_translate' ); ?>
                <button id="click">Закзать звонок</button>
            </article>
        </div>
    </div>
    <nav>
                <?php $args = array(
                'theme_location'  => 'additionalmenu',
                'menu'            => '', 
                'container'       => 'ul', 
                'container_class' => '', 
                'container_id'    => '',
                'menu_class'      => 'navigation', 
                'menu_id'         => '',
                'echo'            => true,

            );
            ?>
            <?php wp_nav_menu( $args ); ?>
    </nav>
</header>
       