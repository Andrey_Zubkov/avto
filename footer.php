<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage kunev
 *
 */

?>
<?php wp_footer(); ?>
<footer>
    <?php $info_settings = get_option('main_option'); ?>
    <div class="wrapper holder">
        <div class="flex-container">
            <article class="flex-item">
                <h3>Информация</h3>
                <p><a href="#">Автомобили</a><br>
                    <a href="#">Услуги</a><br>
                    <a href="#">Условия</a><br>
                    <a href="#">Стоимость</a><br>
                    <a href="#">Контакты</a><br>
                    <a href="#">Отзывы</a>
                </p>
            </article>
            <article class="flex-item">
                <h3>Другие</h3>
                <p><a href="#">Контакыт</a><br>
                    <a href="#">Такси</a><br>
                </p>
            </article>
            <article class="flex-item">
                <h3>Способы оплаты</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/images/visa-logo.png" class="fix-cards">
                <img src="<?php echo get_template_directory_uri(); ?>/images/mastercards-logo.png" class="fix-cards">
            </article>
            <article class="flex-item">
                <h3>Наши контакты</h3>
                <p>Tel: <?php echo $info_settings['phone_1']; ?><br>
                    Email: <?php echo $info_settings['email']; ?><br>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <?php echo $info_settings['work']; ?><br>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <?php echo $info_settings['address']; ?><br></p>
            </article>
        </div>
    </div>
</footer>

</body>
</html>