<?php
load_theme_textdomain( 'avto_jt', get_template_directory() . '/languages' );
require(__DIR__.'/core/create-avto.php');
require(__DIR__.'/core/settings-themes.php');
/** add javascript **/
add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js');
	wp_enqueue_script( 'modal', get_template_directory_uri() . '/js/modalWindow.js');
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick/slick.js');
	wp_enqueue_script( 'slickPlugin', get_template_directory_uri() . '/js/slickPlugin.js');
	wp_enqueue_script( 'upScroll', get_template_directory_uri() . '/js/upScrollPlugin.js');

    wp_enqueue_media();
}

/** add css **/
add_action( 'wp_print_styles', 'add_styles' );
function add_styles() {
	wp_enqueue_style( 'main', 				get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'index', 				get_template_directory_uri().'/css/index.css' );
	wp_enqueue_style( 'fonts', 				get_template_directory_uri().'/css/fonts.css' );
	wp_enqueue_style( 'slick', 				get_template_directory_uri().'/css/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 				get_template_directory_uri().'/css/slick/slick-theme.css' );
	wp_enqueue_style( 'fonts-awesome', 				get_template_directory_uri().'/fonts/font-awesome-4.7.0/css/font-awesome.css' );


}

add_theme_support( 'post-thumbnails' ); # add thubnails

register_nav_menus( array(
    'additionalmenu' => __( 'Main Menu', 'avto' )
) );

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'header_translate',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
    ));

