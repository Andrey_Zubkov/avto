<?php
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage g-r
 */


/** send main **/

get_header(); ?>

<main>
    <div class="banner-main">
        <div class="slider-move">
            <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/images/banner.jpg"></div>
            <div class="item"><img
                        src="<?php echo get_template_directory_uri(); ?>/images/911-rain-city-lights-road-buldings.jpg">
            </div>
            <div class="item"><img
                        src="<?php echo get_template_directory_uri(); ?>/images/land-rover-range-rover-velar-2017-sand.jpg">
            </div>
        </div>
        <div class="wrapper holder">
            <div class="search-panel">
                <select>
                    <option>Категории</option>
                </select>
                <select>
                    <option>Марка</option>
                </select>
                <select>
                    <option>Дата подачи</option>
                </select>
                <select>
                    <option>Дата возврата</option>
                </select>
                <input type="button" value="Поиск">
            </div>
        </div>
        <div class="left-arrow holder"><img src="<?php echo get_template_directory_uri(); ?>/images/left-arrow.png">
        </div>
        <div class="right-arrow holder"><img src="<?php echo get_template_directory_uri(); ?>/images/right-arrow.png">
        </div>
    </div>
    <div class="wrapper holder">

        <div id="bgModal">
            <div id="modal">
                <div class="modal-wrapper">
                    <span id="close">X</span>
                    <h2>Заказать звонок</h2>
                    <label for="name">Имя</label><br>
                    <input type="text" placeholder="Введтие Ваше имя" name="name"><br>
                    <label for="phone">Телефон</label><br>
                    <input type="text" placeholder="Введите номер телефона" name="phone"><br>
                    <input type="submit" value="Заказать">
                </div>
            </div>
        </div>
    </div>

    <section id="category">
        <div class="wrapper holder">
            <h2>Категория</h2>
            <p>Выберай свою категорию</p>
        </div>
        <div class="img-centre">
            <img src="<?php echo get_template_directory_uri(); ?>/images/BgCarOne.png">
        </div>
        <div id="chooseCar">
            <div class="car-style"><img src="<?php echo get_template_directory_uri(); ?>/images/car-one.png"></div>
            <div class="car-style"><img src="<?php echo get_template_directory_uri(); ?>/images/car-two.png"></div>
            <div class="car-style"><img src="<?php echo get_template_directory_uri(); ?>/images/car-three.png">
            </div>
        </div>
        <div class="left-arrow-center holder"><img
                    src="<?php echo get_template_directory_uri(); ?>/images/left-arrow.png"></div>
        <div class="right-arrow-center holder"><img
                    src="<?php echo get_template_directory_uri(); ?>/images/right-arrow.png"></div>
        <p class="cars-category">Средний</p>
    </section>

    <section id="information" class="holder">
        <div class="img-right">
            <div class="info">
                <h2>Аренда авто в Москве</h2>
                <p>Москва - город контрастов, ярких красок, привычной всем суеты. Современный мегаполис, постоянно
                    куда-то стремящийся. В таком месте непросто решить в короткие сроки, куда/на чем отправиться.
                    Такси? Бывает слишком дорогим. Метро? Маршрутки? Слишком долго и не комфортно. Компания "VProkat"
                    предлагает идеальный выход из ситуации − аренда авто в Москве. Для людей, постоянно занятых своим
                    делом, прокат кара −
                    достойная замена столичному такси, a также отличная возможность акцентировать внимание людей на
                    своём статусе. Для туристов же взять автомобиль напрокат − значит получить возможность увидеть все
                    достопримечательности
                    столицы своими глазам по своему расписанию,
                    a не по навязанным экскурсоводами системам.</p>
            </div>
            <img src="<?php echo get_template_directory_uri(); ?>/images/BgCarTwo.png">
        </div>
        <div class="img-left">

            <img src="<?php echo get_template_directory_uri(); ?>/images/BgCarThree.png">
            <div class="info-centre">
                <h2>Аренда авто для<br> Европы!</h2>
                <p>
                    РФ, Белоруссия , Европа. Запрещен выезд в Ингушетию, Дагестан и Чеченскую Республику. При выезде за
                    пределы РФ дополнительно оплачивается расширение территории страхования КАСКО в соответствии с
                    тарифным планом; Суточный пробег 300 км.

                    Перепробег оплачивается отдельно согласно установленным тарифам.
                </p>
            </div>
        </div>
        <div class="img-right">
            <div class="info-down">
                <h2>Аренда авто под такси <br> без залога!</h2>
                <p>В нашем автопарке находятся только лучшие модели автомобилей, включая бизнес-класс, возрастом до 2
                    лет. Можно выбрать «Хундай Солярис»,
                    «Форд Фокус 3», «Хендай Элантра» и другие авто с автоматической и механической коробкой передач.
                    Комфортабельная, ухоженная машина – показатель высокого качества услуг.
                    Прокатившись однажды, клиенты захотят и впредь обращаться только к вам.
                    Кстати, мы предлагаем сниженный процент комиссии на собственные заказы.</p>
            </div>
            <img class="last-img" src="<?php echo get_template_directory_uri(); ?>/images/BgCraFour.png">
        </div>
        <div class="scrollTop">
            <a href="#top" id="up"><img src="<?php echo get_template_directory_uri(); ?>/images/scrollTop.png"></a>
        </div>
    </section>
    </div>


</main>
<?php get_footer(); ?>    

					